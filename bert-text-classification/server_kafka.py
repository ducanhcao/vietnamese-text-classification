from fastapi import FastAPI
from pydantic import BaseModel
import torch
import torch.nn as nn

from transformers import AutoModel, AutoTokenizer

from kafka import KafkaConsumer, KafkaProducer
import json

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# device = 'cpu'

tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base", use_fast=False)

classes = ['xe_360', 'sống_trẻ', 'nhịp_sống', \
'thời_trang', 'phim_ảnh', 'sức_khỏe', \
'thể_thao', 'kinh_doanh', 'xuất_bản', \
'du_lịch', 'thời_sự', 'giải_trí', \
'ẩm_thực', 'âm_nhạc', 'thế_giới', \
'giáo_dục', 'công_nghệ', 'pháp_luật']

class BERTClassifier(nn.Module):
    def __init__(self, n_classes):
        super(BERTClassifier, self).__init__()
        self.bert = AutoModel.from_pretrained("vinai/phobert-base")
        self.drop = nn.Dropout(p=0.3)
        self.fc = nn.Linear(self.bert.config.hidden_size, n_classes)
        nn.init.normal_(self.fc.weight, std=0.02)
        nn.init.normal_(self.fc.bias, 0)

    def forward(self, input_ids, attention_mask):
        last_hidden_state, output = self.bert(
            input_ids=input_ids,
            attention_mask=attention_mask,
            return_dict=False # Dropout will errors if without this
        )

        x = self.drop(output)
        x = self.fc(x)
        return x

def infer(text, model, tokenizer, class_names, max_len=256):
    encoded_review = tokenizer.encode_plus(
        text,
        max_length=max_len,
        truncation=True,
        add_special_tokens=True,
        padding='max_length',
        return_attention_mask=True,
        return_token_type_ids=False,
        return_tensors='pt',
    )

    input_ids = encoded_review['input_ids'].to(device)
    attention_mask = encoded_review['attention_mask'].to(device)

    output = model(input_ids, attention_mask)
    _, y_pred = torch.max(output, dim=1)

    print(f'Text: {text}')
    # print(f'encoded_review: {encoded_review}')
    print(f'Class: {class_names[y_pred]}')
    return class_names[y_pred]


model = BERTClassifier(n_classes=18)
model.to(device)
model.load_state_dict(torch.load('/home/anhcd/Projects/bert-text-classification/weights_topic/phobert_fold5.pth'))
model.eval()

# Kafka configuration ================================================================================================
bootstrap_servers = 'localhost:9094'
input_topic_name = 'request_nlp'
output_topic_name = 'response_nlp'

# Function to process the task and paragraph using deep learning logic
def process_paragraph(paragraph):
    # Dummy processing logic
    # processed_label = request.paragraph + "something"  # Replace with actual processing logic
    processed_label = infer(paragraph, model, tokenizer, classes)

    return processed_label

# Function to consume messages from Kafka, process them, and produce results
def consume_and_produce_messages(bootstrap_servers, input_topic_name, output_topic_name):
    consumer = KafkaConsumer(
        input_topic_name,
        bootstrap_servers=bootstrap_servers,
        auto_offset_reset='earliest',  # Start reading at the earliest available message
        enable_auto_commit=True,
        group_id='dac-group',
        value_deserializer=lambda m: json.loads(m.decode('utf-8'))
    )
    
    producer = KafkaProducer(
        bootstrap_servers=bootstrap_servers,
        value_serializer=lambda v: json.dumps(v).encode('utf-8')
    )
    
    for message in consumer:
        print(f"Received message: {message.value}")
        task_name = message.value.get("task_name")
        paragraph = message.value.get("content", {}).get("paragraph")
        task_id = message.value.get("task_id", 0)  # Assuming task_id is part of the message
        
        if task_name == "text_classify":
            result = process_paragraph(paragraph)
            response = {"task_id": task_id, "result": {"label":result}}
            print(f"Sending response: {response}")
            producer.send(output_topic_name, value=response)
            producer.flush()
        else:
            print("Invalid message format")

# Start consuming and producing messages
consume_and_produce_messages(bootstrap_servers, input_topic_name, output_topic_name)


