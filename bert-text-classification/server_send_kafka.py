from kafka import KafkaProducer
from kafka.admin import KafkaAdminClient, NewTopic
from kafka.errors import TopicAlreadyExistsError
import json

bootstrap_servers = 'localhost:9094'
input_topic_name = 'request_nlp'
output_topic_name = 'response_nlp'
message = {
    "task_name": "text_classify",
    "task_id": "2",
    "content": {
        "paragraph": "Giáo dục trẻ nhỏ"
    }
}

# Function to create a Kafka topic if it does not exist
def create_topic_if_not_exists(bootstrap_servers, topic_name):
    admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)
    
    # Check if the topic already exists by trying to create it
    topic = NewTopic(name=topic_name, num_partitions=1, replication_factor=1)
    try:
        admin_client.create_topics([topic])
        print(f"Topic '{topic_name}' created")
    except TopicAlreadyExistsError:
        print(f"Topic '{topic_name}' already exists")
    finally:
        admin_client.close()

# Function to send a message to the Kafka topic
def send_message_to_topic(bootstrap_servers, topic_name, message):
    producer = KafkaProducer(bootstrap_servers=bootstrap_servers,
                             value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    
    # Produce the message to the topic
    producer.send(topic_name, value=message)
    producer.flush()
    producer.close()
    print(f"Message sent to topic '{topic_name}'")

# Create the topic if it does not exist
create_topic_if_not_exists(bootstrap_servers, input_topic_name)
create_topic_if_not_exists(bootstrap_servers, output_topic_name)

# Send the JSON message to the topic
send_message_to_topic(bootstrap_servers, input_topic_name, message)