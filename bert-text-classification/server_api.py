from fastapi import FastAPI
from pydantic import BaseModel
import torch
import torch.nn as nn

from transformers import AutoModel, AutoTokenizer

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# device = 'cpu'

tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base", use_fast=False)

classes = ['xe_360', 'sống_trẻ', 'nhịp_sống', \
'thời_trang', 'phim_ảnh', 'sức_khỏe', \
'thể_thao', 'kinh_doanh', 'xuất_bản', \
'du_lịch', 'thời_sự', 'giải_trí', \
'ẩm_thực', 'âm_nhạc', 'thế_giới', \
'giáo_dục', 'công_nghệ', 'pháp_luật']

class BERTClassifier(nn.Module):
    def __init__(self, n_classes):
        super(BERTClassifier, self).__init__()
        self.bert = AutoModel.from_pretrained("vinai/phobert-base")
        self.drop = nn.Dropout(p=0.3)
        self.fc = nn.Linear(self.bert.config.hidden_size, n_classes)
        nn.init.normal_(self.fc.weight, std=0.02)
        nn.init.normal_(self.fc.bias, 0)

    def forward(self, input_ids, attention_mask):
        last_hidden_state, output = self.bert(
            input_ids=input_ids,
            attention_mask=attention_mask,
            return_dict=False # Dropout will errors if without this
        )

        x = self.drop(output)
        x = self.fc(x)
        return x

def infer(text, model, tokenizer, class_names, max_len=256):
    encoded_review = tokenizer.encode_plus(
        text,
        max_length=max_len,
        truncation=True,
        add_special_tokens=True,
        padding='max_length',
        return_attention_mask=True,
        return_token_type_ids=False,
        return_tensors='pt',
    )

    input_ids = encoded_review['input_ids'].to(device)
    attention_mask = encoded_review['attention_mask'].to(device)

    output = model(input_ids, attention_mask)
    _, y_pred = torch.max(output, dim=1)

    print(f'Text: {text}')
    # print(f'encoded_review: {encoded_review}')
    print(f'Class: {class_names[y_pred]}')
    return class_names[y_pred]


model = BERTClassifier(n_classes=18)
model.to(device)
model.load_state_dict(torch.load('/home/anhcd/Projects/bert-text-classification/weights_topic/phobert_fold5.pth'))
model.eval()

app = FastAPI()

class ParagraphRequest(BaseModel):
    paragraph: str

class LabelResponse(BaseModel):
    label: str

@app.post("/text-classify", response_model=LabelResponse)
async def process_paragraph(request: ParagraphRequest):
    # Dummy processing logic
    # processed_label = request.paragraph + "something"  # Replace with actual processing logic
    processed_label = infer(request.paragraph, model, tokenizer, classes)

    return LabelResponse(label=processed_label)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8001)
